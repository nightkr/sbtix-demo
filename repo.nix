{
  "sbtix-demo" = {
    "org.scala-lang"."scala-library"."2.10.6" = {
      "jar" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-library/2.10.6/scala-library-2.10.6.jar";
        sha256 = "0fy2i38z5x4xhw70rlsj33ddg225f15jr9ldbdq3gh59d1fvzqkr"";
      };
      "pom" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-library/2.10.6/scala-library-2.10.6.pom";
        sha256 = "0rr50x93xg480961s0ppyfd5ilcrmdw3rdz29y3dfnrmap99vl34"";
      };
    };
    "org.scala-lang"."scala-reflect"."2.10.6" = {
      "jar" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-reflect/2.10.6/scala-reflect-2.10.6.jar";
        sha256 = "0vgdn6mzzxqwrai1hsjcpsjxqnn38nv1caw6mhmwh3q4zq8jd7xb"";
      };
      "pom" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-reflect/2.10.6/scala-reflect-2.10.6.pom";
        sha256 = "1085xmln652gzf3dn2hvyhnfx9pnwsipb54jr5rdxqm30pb1p8i3"";
      };
    };
    "org.scala-lang"."scala-compiler"."2.10.6" = {
      "jar" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-compiler/2.10.6/scala-compiler-2.10.6.jar";
        sha256 = "0fzwhlw9pl92b3mgci85qqz8s0fdci9dyh28lnjcdcjxpqrmjbpb"";
      };
      "pom" = {
        url = "https://repo1.maven.org/maven2/org/scala-lang/scala-compiler/2.10.6/scala-compiler-2.10.6.pom";
        sha256 = "0spli5785blg6q9rhgh10vgwk01v69zs3ncn1sam5rzbwpkhashi"";
      };
    };
  };
}
